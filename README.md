# Shimmy #

This set of Shimmy Dojo widgets extend the standard Dijit widgets with internal
IFrame shims that accurately track the size and position of the widgets.
They are particularly useful for displaying fully functional moveable widgets 
over embedded Google Earth plugin frames.

## Usage ##

1. place the shimmy directory as a child of the dojo directory;
2. add the shimmy package to data-dojo-config;
3. use the type 'shimmy/Dialog' instead of 'dijit/Dialog'.

Thanks to Roman Nurik for his Google Earth shim sample:
[http://earth-api-samples.googlecode.com/svn/trunk/demos/customcontrols/index.html](http://earth-api-samples.googlecode.com/svn/trunk/demos/customcontrols/index.html)

Copyright (c) 2012, Oliver Lade

All rights reserved

The Shim Dialog is released under to following two licenses:

1. The 'New' BSD License		([http://trac.dojotoolkit.org/browser/dojo/trunk/LICENSE#L13](http://trac.dojotoolkit.org/browser/dojo/trunk/LICENSE#L13))
2. The Academic Free License	([http://trac.dojotoolkit.org/browser/dojo/trunk/LICENSE#L43](http://trac.dojotoolkit.org/browser/dojo/trunk/LICENSE#L43))

         o/`
        This, my shim,
        Is there to please
        Strip the hole
        Fill them all
         o/`